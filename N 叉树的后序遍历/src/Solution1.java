import java.util.ArrayList;
import java.util.List;

public class Solution1 {
    public static List<Integer> postOrder(Node root){
        List<Integer> res = new ArrayList<>();
        lastPre(root,res);
        return res;
    }

    private static void lastPre(Node root, List<Integer> res) {
        if (root != null){
            return;
        }
        for(Node node : root.getChildren()){
            lastPre(node,res);
        }
        res.add(root.getVal());
    }

    public static void main(String[] args) {
        Node root = new Node();
        postOrder(root);
    }
/**
    复杂度分析

    时间复杂度：O(m)O(m)，其中 mm 为 NN 叉树的节点。每个节点恰好被遍历一次。

    空间复杂度：O(m)O(m)，递归过程中需要调用栈的开销，平均情况下为 O(\log m)O(logm)，最坏情况下树的深度为 m-1m−1，需要的空间为 O(m-1)O(m−1)，因此空间复杂度为 O(m)O(m)。。

    方法二：迭代思路
 **/

}
