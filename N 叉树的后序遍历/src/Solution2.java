import java.util.*;

/**
 代码
 **/
class Solution2 {
    public List<Integer> postorder(Node root) {
        List<Integer> res = new ArrayList<Integer>();
        if (root == null) {
            return res;
        }
        Map<Node, Integer> map = new HashMap<Node, Integer>();
        Deque<Node> stack = new ArrayDeque<Node>();
        Node node = root;
        while (!stack.isEmpty() || node != null) {
            while (node != null) {
                stack.push(node);
                List<Node> children = node.getChildren();
                if (children != null && children.size() > 0) {
                    map.put(node, 0);
                    node = children.get(0);
                } else {
                    node = null;
                }
            }
            node = stack.peek();
            int index = map.getOrDefault(node, -1) + 1;
            List<Node> children = node.getChildren();
            if (children != null && children.size() > index) {
                map.put(node, index);
                node = children.get(index);
            } else {
                res.add(node.getVal());
                stack.pop();
                map.remove(node);
                node = null;
            }
        }
        return res;
    }


/**
 方法一中利用递归来遍历树，实际的递归中隐式利用了栈，在此我们可以直接模拟递归中栈的调用。在后序遍历中从左向右依次先序遍历该每个以子节点为根的子树，然后先遍历节点本身。

 在这里的栈模拟中比较难处理的在于从当前节点 uu 的子节点 v_1v

 返回时，此时需要处理节点 uu 的下一个节点 v_2v

 此时需要记录当前已经遍历完成哪些子节点，才能找到下一个需要遍历的节点。

 在二叉树树中因为只有左右两个子节点，因此比较方便处理，在 NN 叉树中由于有多个子节点，

 因此使用哈希表记录当前节点 uu 已经访问过哪些子节点。

 每次入栈时都将当前节点的 uu 的第一个子节点压入栈中，直到当前节点为空节点为止。

 每次查看栈顶元素 pp，如果节点 pp 的子节点已经全部访问过，则记录当前节点的值，

 并将节点 pp 的从栈中弹出，并从哈希表中移除，表示该以该节点的子树已经全部遍历过；

 如果当前节点 pp 的子节点还有未遍历的，则将当前节点的 pp 的下一个未访问的节点压入到栈中，

 重复上述的入栈操作。
 **/


/*
复杂度分析

时间复杂度：O(m)O(m)，其中 mm 为 NN 叉树的节点。每个节点恰好被访问一次。

空间复杂度：O(m)O(m)，其中 mm 为 NN 叉树的节点。如果 NN 叉树的深度为 11 则此时栈和哈希表的空间为 O(1)O(1)，

如果 NN 叉树的深度为 m-1m−1 则此时栈和哈希表的空间为 O(m-1)O(m−1)，平均情况下栈和哈希表的空间为 O(\log m)O(logm)，因此空间复杂度为 O(m)O(m)。

* */

 }